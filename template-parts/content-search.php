<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
	$selectMedia   	= get_field('select_media'); 
	$postLink  		= get_post_permalink();
	$theTitle      	= get_the_title();
	$audioURL   	= get_field('post_audio'); 
	$videoURL       = get_field('post_video');
	$imageURL   	= get_field('featured_image');
	$videoImageURL  = get_field('featured_video_image'); 
	$audioImageURL 	= get_field('featured_audio_image');
	// $terms   		= get_the_terms(get_the_ID(), $taxonomySlug); 
	$currentUserId	= get_current_user_id();
	$postID   		= get_the_ID();
	$queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
	$results 		= $wpdb->get_results($queryDB); 
	$taxonomies 	= get_taxonomies(); 
	$terms			= wp_get_post_terms($postID, $taxonomies );
	if (is_array($terms) || is_object($terms)){
		foreach($terms as $term) { 
			$term_id = $term->term_id; 
			$child_term = get_term( $term_id, $taxonomySlug );
			$parent_term = get_term( $child_term->parent, $taxonomySlug );
			$child_term_name = $child_term->name;
			$parent_term_name = $parent_term->name;
		}  
	} 
?>
 
	<?php if ( 'kitchen' === get_post_type() || 'songs' === get_post_type() || 'health' === get_post_type() || 'fashion' === get_post_type()  || 'cinema' === get_post_type()  || 'life' === get_post_type()  || 'islamic' === get_post_type()  ) : 
		
		
	$postType 		= get_queried_object()->post_type;
	?>
 
		<div class="contentData">
			<div class="position-relative imageHover">
				<?php if($selectMedia == "videoFile" ){ ?>
					<img src="<?php echo $videoImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
				<?php } elseif($selectMedia == "imageFile"){ ?>
					<img src="<?php echo $imageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
				<?php } elseif($selectMedia == "audioFile"){ ?>
					<img src="<?php echo $audioImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
				<?php } ?>
				<div class="hoverItem w-100">
					<div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
						<?php if($selectMedia == "videoFile"){ ?>
							<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
								<i aria-hidden="true" class="fa fa-play"></i>
							</a> 
							<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $videoImageURL; ?> ">
								<i aria-hidden="true" class="fa fa-heart"></i>
							</a> 
						<?php } elseif($selectMedia == "imageFile"){ ?>
							<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
								<i aria-hidden="true" class="fa fa-eye"></i>
							</a> 
							<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $imageURL; ?> ">
								<i aria-hidden="true" class="fa fa-heart"></i>
							</a> 
						<?php } elseif($selectMedia == "audioFile"){ ?>
							<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
								<i aria-hidden="true" class="fa fa-play"></i>
							</a> 
							<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $audioImageURL; ?> ">
								<i aria-hidden="true" class="fa fa-heart"></i>
							</a> 
						<?php } ?>
					</div> 
				</div> 
			</div>
			<div class="itemDetails text-break">
				<a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
				
			</div>
		</div>  

	<?php else :  ?>

		<!-- <div class="d-flex justify-content-center py-3">
			<h6><?php // echo __('No results found, please try again.') ?></h6>
		</div> -->

	<?php endif; ?> 
