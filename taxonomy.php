<?php 
    get_header();
    global $wpdb;
    $taxonomy     = get_queried_object();
    $taxonomyID   = $taxonomy->term_id; 
    $taxonomyName = $taxonomy->name;
    $videoContent = false;
    $audioContent = false;
    $imageContent = false; 
    $siteLanguage = get_locale();
    $RTL 		= $siteLanguage == "ar" ? true : false ;
?>

    <div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <?php if ( ! is_user_logged_in() ) { ?>
			<div class="container">
				<div class="row py-5">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					<?php if($RTL){ ?>
						<span class="alert alert-danger text-right w-100 float-right">
							يجب
							<a class='alert-link' 
								href="<?php  echo pll_get_page_url('/subscribe') ?>">
								الاشتراك او تسجيل الدخول
							</a>
							لمشاهدة المحتوى
						</span>
					<?php } else{ ?>
						<span class="alert alert-danger text-left w-100 float-left">
							You should
							<a class='alert-link' 
								href="<?php echo pll_get_page_url('/subscribe') ?>">
								login or subscribe
							</a>
							to view content
						</span>
					<?php } ?>
                    </div>
				</div>
			</div>
				<?php
			} else { ?>
            <div class="container">
                <div class="row">
                    <div class="col-12 mt-4">
                        <div class="mainTitle">
                            <h2><?php echo __( $taxonomyName ) ?></h2>
                        </div>
                    </div>
                </div>
            </div>
		<?php if ( have_posts() ) : ?>

			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post(); 
                $selectMedia   = get_field('select_media');
                $postID   		= get_the_ID();  
                $currentUserId	= get_current_user_id();
                $queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
                $results 		= $wpdb->get_results($queryDB); 

                if($selectMedia == "videoFile"){ 
                    $videoContent = true;
                } 
                if($selectMedia == "audioFile"){ 
                    $audioContent = true;
                } 
                if($selectMedia == "imageFile"){ 
                    $imageContent = true;
                } 

				// End the loop.
            endwhile; 
            
			// If no content, include the "No posts found" template.
        else :  
			get_template_part( 'template-parts/content', 'none' ); 
		endif;
        ?>
        
        <?php 
            // taxonomyLayout ($taxonomyName, $tabID, $iconClass, $sectionBg, $termType)
            if($videoContent == true){ 
                taxonomyLayout ($taxonomyName, 'video', 'fa-play', '', 'video');  
            }

            if($audioContent == true){
                taxonomyLayout ($taxonomyName, 'audio', 'fa-play', 'grayBG', 'audio');
            }

            if($imageContent == true){
                taxonomyLayout ($taxonomyName, 'image', 'fa-eye', '', 'image');
            }
        }
        ?>
		</main><!-- .site-main -->
    </div><!-- .content-area -->

<?php 
    get_footer();

    function taxonomyLayout ($taxonomyName, $tabID, $iconClass, $sectionBg, $termType) { ?>
        <?php
            if($termType == "video"){
                $tabTitle 		 = "Video";
                $selectMediaType = "videoFile" ;
                $mediaSlug 		 = "post_video";
                $thumbnailSlug 	 = "featured_video_image";
            }
            if($termType == "audio"){
                $tabTitle 		 = "Audio";
                $selectMediaType = "audioFile" ;
                $mediaSlug 		 = "post_audio";
                $thumbnailSlug 	 = "featured_audio_image";
            }
            if($termType == "image"){
                $tabTitle 		 = "Photo";
                $selectMediaType = "imageFile" ;
                $mediaSlug 		 = "featured_image";
                $thumbnailSlug 	 = "featured_image";
            }
        ?>
        <section class="<?php echo $sectionBg ?>">
            <div class="container">
                <div class="row">
                    <div id="tabs" class="col-12 py-4 mt-0">
    
                        <ul class="nav tablist" > 
                            <li class="nav-item ">
                                <a href="#<?php echo $tabID; ?>" id="<?php echo $tabID; ?>" class="nav-link" role="tab" data-toggle="tab"> 
                                    <h2><?php echo __( $tabTitle ) ?></h2> 
                                </a> 
                            </li>
                            <!-- <span></span> -->
                        </ul>
                        <div class="tab-content"> 
                            <!-- <a href="#" class="moreBtn"><?php// echo __('More') ?></a>  -->
                            <div id="<?php echo $tabID; ?>" role="tabpanel" class="tab-pane slickSlider videoContent">
                    <?php
                    if ( have_posts() ) :
                        // Start the Loop.
                        while ( have_posts() ) :
                            the_post(); 
                            global $wpdb; 
                            $selectMedia   	= get_field('select_media');   
                            $thumbnailURL 	= get_field( $thumbnailSlug );
                            $mediaURL   	= get_field( $mediaSlug ); 
                            $postLink  		= get_post_permalink();
                            $postID   		= get_the_ID(); 
                            $theTitle       = get_the_title(); 
                            $currentUserId	= get_current_user_id();
                            $queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
                            $results 		= $wpdb->get_results($queryDB);
    
                            if($mediaURL && $selectMedia == $selectMediaType) {
                                
                                ?>
                                <div class="contentData">
                                    <div class="position-relative imageHover">
                                        <img src="<?php echo $thumbnailURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
                                        <div class="hoverItem w-100">
                                            <div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
                                                <a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
                                                    <i aria-hidden="true" class="fa <?php echo $iconClass ?>"></i>
                                                </a> 
												<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $thumbnailURL; ?> ">
													<i aria-hidden="true" class="fa fa-heart"></i>
												</a> 
                                            </div> 
                                        </div> 
                                    </div>
                                    <div class="itemDetails text-break">
                                        <a href="<?php echo $postLink ?>"><?php echo the_title() ?></a> 
                                        <p><?php echo $taxonomyName ?></p>  
                                    </div>
                                </div> 
    
                                <?php
                            }
                                // End the loop.
                            endwhile;
                            endif;
                            ?>  
                                
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>
        </section>
    
        <?php
    } 
?>