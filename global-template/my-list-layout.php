<?php
/* Template Name: My-List */ 
    get_header();
	global $wpdb;
    $currentUserId = get_current_user_id();
    $siteLanguage 	= get_locale();
    $RTL 			= $siteLanguage == "ar" ? true : false ;
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    <?php if ( ! is_user_logged_in() ) { ?>
			<div class="container">
				<div class="row py-5">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					<?php if($RTL){ ?>
						<span class="alert alert-danger text-right w-100 float-right">
							يجب
							<a class='alert-link' 
								href="<?php  echo pll_get_page_url('/subscribe') ?>">
								الاشتراك او تسجيل الدخول
							</a>
							لمشاهدة المحتوى
						</span>
					<?php } else{ ?>
						<span class="alert alert-danger text-left w-100 float-left">
							You should
							<a class='alert-link' 
								href="<?php echo pll_get_page_url('/subscribe') ?>">
								login or subscribe
							</a>
							to view content
						</span>
					<?php } ?>
                    </div>
				</div>
			</div>
				<?php
			} else { ?>

        <div class="container">
            <header class="entry-header py-3 w-100">
                <?php the_title( '<h1 class="m-0">', '</h1>' ); ?>
            </header><!-- .entry-header -->
 
            <?php 
                $userLikesResultDB 	= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  ;
                $userLikesResults   = $wpdb->get_results($userLikesResultDB);
                //  var_dump($userLikesResults);
                if ($userLikesResults) { ?>

                <div role="tabpanel" class="userList my-0 tab-pane d-flex flex-wrap"> 
                <?php
                    foreach($userLikesResults as $userLike) {
                    // var_dump($userLike);
                    $userID = $userLike->fav_user_id;
                    $postID = $userLike->fav_post_id;
                    $postLink = $userLike->fav_post_url;
                    $postTitle = $userLike->fav_post_title;
                    $postImage = $userLike->fav_post_image;
                ?>

                    <div class="contentData">
                        <div class="position-relative imageHover">
                            <img src="<?php echo $postImage; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
                            <div class="hoverItem w-100">
                                <div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
                                    <a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
                                        <i aria-hidden="true" class="fa fa-eye"></i>
                                    </a> 
                                    <a class="sFavrt-item liked" href="#" data-user-id="<?php echo $userID; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $postTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $postImage; ?> ">
                                        <i aria-hidden="true" class="fa fa-heart"></i>
                                    </a>   
                                </div> 
                            </div> 
                        </div>
                        <div class="itemDetails text-break">
                            <a href="<?php echo $postLink ?>"><?php echo $postTitle; ?></a>

                        </div>
                    </div>

                    <?php } ?> 
                </div>
                <?php
                } else { ?>
                    <div class="noLikes">
                        <p></p>
                    </div>
                <?php } 
            ?> 
 
        </div>

        <?php } ?>
    </main><!-- .site-main -->
</div><!-- .content-area --> 
<?php 
    get_footer(); 
?> 