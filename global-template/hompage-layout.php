<?php
session_start();  
require (get_template_directory().'/he_file.php');
$login = ( is_user_logged_in() ? "true" : "false");
$modal = (!is_user_logged_in() ? "openModal" : "");
$is_registed = (username_exists($ani)? "true" : "false"); 

if( $_SESSION['previous_location'] !== 'landingPage' && !is_user_logged_in()){ 
    wp_redirect( site_url() );  
} 
/* Template Name: Homepage */ 
    get_header(); 
    $siteLanguage 	= get_locale();
    $RTL 	= $siteLanguage == "ar" ? true : false ;
?>
 
<section class="hero_section">
    <div class="container-fluid">
        <div class="homeSlider pt-5">
            <?php 
 
            ?>
            <div class="slideUnit <?php echo $modal; ?>">
                <a href="<?php echo $RTL ? home_url( '/ar/cinema') : home_url( '/cinema') ?>">
                    <img src="<?php echo get_template_directory_uri()?>/img/Cinma_thumb.png"/>
                </a>
            </div>

            <div class="slideUnit <?php echo $modal; ?>">
                <a href="<?php echo $RTL ? home_url( '/ar/cinema') : home_url( '/cinema') ?>">
                    <img src="<?php echo get_template_directory_uri()?>/img/Qafashat_thumb.png"/>
                </a>
            </div>

            <div class="slideUnit <?php echo $modal; ?>">
                <a href="<?php echo $RTL ? home_url( '/ar/islamic') : home_url( '/islamic') ?>">
                    <img src="<?php echo get_template_directory_uri()?>/img/Quran_thumb.png"/>
                </a>
            </div>

            <div class="slideUnit <?php echo $modal; ?>">
                <a href="<?php echo $RTL ? home_url( '/ar/health') : home_url( '/health') ?>">
                    <img src="<?php echo get_template_directory_uri()?>/img/tabeb.png"/>
                </a>
            </div>

            <div class="slideUnit <?php echo $modal; ?>">
                <a href="<?php echo $RTL ? home_url( '/ar/life') : home_url( '/life') ?>">
                    <img src="<?php echo get_template_directory_uri()?>/img/Tanmyh_thumb.png"/>
                </a>
            </div>

        </div>
    </div>
</section> 

<?php
// function homeLayout ($postTypeName, $sectionTitle, $firstTab, $secondTab, $thirdTab, $taxonomySlug, $sectionBg, $modal)

    homeLayout('kitchen', __('Kitchen'), 'kitchen_video', 'kitchen_audio', 'kitchen_image', 'kitchen_taxonomy', '',$modal );

    homeLayout('songs', __('Songs'), 'songs_video', 'songs_audio', 'songs_image', 'songs_taxonomy', 'grayBG',$modal);

    homeLayout('health', __('Health'), 'health_video', 'health_audio', 'health_image', 'health_taxonomy', '',$modal);

    homeLayout('fashion', __('Fashion'), 'fashion_video', 'fashion_audio', 'fashion_image', 'fashion_taxonomy', 'grayBG',$modal);
    
    // homeLayout('cinema', __('Cinema'), 'cinema_video', 'cinema_audio', 'cinema_image', 'cinema_taxonomy', '',$modal);

    // homeLayout('life', __('Life'), 'life_video', 'life_audio', 'life_image', 'life_taxonomy', 'grayBG',$modal);
    
    // homeLayout('islamic', __('Islamic'), 'islamic_video', 'islamic_audio', 'islamic_image', 'islamic_taxonomy ', '',$modal);

?>
<?php if(!$ani) { ?>
    <!-- The Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p class="text-center">للتمتع بإمكانية الوصول الكامل إلى 3eeshonline ، انقر فوق الاشتراك مع 1 جنية في اليوم. أو انقر فوق تسجيل الدخول باستخدام كلمة مرور حسابك.</p>
                <img src = "<?php echo get_template_directory_uri()?>/img/main-logo-black.png">
            </div>
            <div class="modal-footer justify-content-center">
                <a href = "<?php echo get_site_url()?>/subscribe" class="login">دخول</a>
                <a href = "<?php echo get_site_url()?>/subscribe" class="subscribe_btn">اشتراك</a>
            </div>
        </div>
    </div>
</div>



<?php } elseif($ani) { ?>
<!-- The Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php if($subStatus == "4"){?>
            <div class="modal-body text-center">     
               <p class="pop_msg text-center msg_validation text-danger">للأسف لا يوجد لديك رصيد كافي من فضلك قم بالشحن و أعد المحاولة</p> 
            </div>
            <?php }elseif($subStatus == "2"){ ?>
                <div class="modal-body text-center">
                <p class="text-center pop_msg">للتمتع بإمكانية الوصول الكامل إلى 3eeshonline انقر فوق اشتراك ب 1 جنية في اليوم.</p>
                <img src = "<?php echo get_template_directory_uri()?>/img/main-logo-black.png"> 
                <div class="alert_msg">
                </div>   
            </div>
            <div class="modal-footer justify-content-center">
                <form method="POST" action=''>
                    <input type="hidden" value="<?php $ani ?>" name="ani" id="ani">
                    <button type="submit" class="subscribtion" name="he_sub">اشتراك</button>
                </form>
            </div>
          <?php  }else{ ?>
            <div class="modal-body text-center">     
               <p class="text-center msg_validation text-danger">عذرا حدث خطأ اثناء الاتصال من فضلك حاول مرة اخرى</p> 
            </div>   
         <?php }?>
            
           
        </div>
    </div>
</div>

<?php }
    get_footer(); 
?>

<script>

jQuery(document).ready(function($){

    let is_Registed = "<?php echo $is_registed ?>"
    let ani = "<?php echo $ani ?>"

    $(".subscribtion").click(function(e){
        e.preventDefault();
		//date_user_meta(true); 
        //have account
        if(is_Registed == "true"){
            // call the function that update user and send sms located in js/update-user-send-sms.js
            update_user_send_sms(ani)
            // check if it fails do that ..
            .fail(error => console.error(error))
            // check if done do that ...
            .done(response => {
            let response_json = JSON.parse(response);
            api_response_code =  response_json; 
            console.log("msg response is " + api_response_code);
            $(".alert_msg").append("<p class='text-centet text-success'>تم التسجيل بنجاح</p>")
            $(".subscribtion").attr("disabled", true);
                setTimeout(function(){ window.location.href = ajax_object.site_url; }, 3000);
            });
            //not have account
        }else{
            // call the function that create user and send sms located in js/create-user-send-sms.js
            create_new_user_send_sms(ani)
            // check if it fails do that ..
            .fail(error => console.error(error))
            // check if done do that ...
            .done(response =>{
                let response_json = JSON.parse(response);
                api_response_code =  response_json; 
                    console.log("msg response is " + api_response_code);
                $(".alert_msg").append("<p class='text-centet text-success'>تم الاشتراك بنجاح جارى تسجيل بياناتك </p>");
                $(".subscribtion").attr("disabled", true);
                    setTimeout(function(){ window.location.href = ajax_object.site_url; }, 3000);
            });

        }
        
    });

});
</script>
