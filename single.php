<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header();
global $wpdb; ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main"> 
		<?php  
		$siteLanguage 	= get_locale();
		$RTL 			= $siteLanguage == "ar" ? true : false ;
		$postID   		= get_the_ID(); 
		// $taxonomies 	= get_taxonomies(); 
		// $terms			= wp_get_post_terms($postID, $taxonomies );
		// var_dump($terms);
		// $taxonomyName   = $terms[2]->taxonomy;
		// $term 			= get_the_terms( $postID, $taxonomyName );
		// $termName   	= $term[0]->name;
		?>
		<?php if ( ! is_user_logged_in() ) { ?>
			<div class="container">
				<div class="row py-5">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					<?php if($RTL){ ?>
						<span class="alert alert-danger text-right w-100 float-right">
							يجب
							<a class='alert-link' 
								href="<?php  echo pll_get_page_url('/subscribe') ?>">
								الاشتراك او تسجيل الدخول
							</a>
							لمشاهدة المحتوى
						</span>
					<?php } else{ ?>
						<span class="alert alert-danger text-left w-100 float-left">
							You should
							<a class='alert-link' 
								href="<?php echo pll_get_page_url('/subscribe') ?>">
								login or subscribe
							</a>
							to view content
						</span>
					<?php } ?>
                    </div>
				</div>
			</div>
				<?php
			} else { 
		$postType 		= get_queried_object()->post_type; 
		// var_dump($postType); 
		if($postType == "kitchen"){
			$term		= wp_get_post_terms($postID, 'kitchen_taxonomy' );
		} elseif($postType == "health"){
			$term		= wp_get_post_terms($postID, 'health_taxonomy' );
		} elseif($postType == "fashion"){
			$term		= wp_get_post_terms($postID, 'fashion_taxonomy' );
		} elseif($postType == "cinema"){
			$term		= wp_get_post_terms($postID, 'cinema_taxonomy' );
		} elseif($postType == "life"){
			$term		= wp_get_post_terms($postID, 'life_taxonomy' );
		} elseif($postType == "islamic"){
			$term		= wp_get_post_terms($postID, 'islamic_taxonomy' );
		} elseif($postType == "songs"){
			$term		= wp_get_post_terms($postID, 'songs_taxonomy' );
		}
		$termName   	= $term[0]->name; 


		// Start the loop. 
		while ( have_posts() ) :
			the_post(); 
			$selectMedia    = get_field('select_media');
			$videoURL	    = get_field('post_video');
			$audioURL 	    = get_field('post_audio');
			$imageURL 	    = get_field('featured_image');
			$audioImageURL  = get_field('featured_audio_image');
			$videoImageURL  = get_field('featured_video_image');
			$currentUserId	= get_current_user_id();
			$postLink		= get_the_permalink(); 
			$queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
			$results 		= $wpdb->get_results($queryDB);
			// echo $selectMedia;
			// echo $videoURL;
			// echo $audioURL;
			// echo $imageURL;
			// echo $audioImageURL;
			// echo $videoImageURL;
			// echo $termName;
			// echo the_title();  
			?>


		<div class="container singleContent"> 
				<div class="col-12 d-flex flex-row"> 
					<div class="col-md-7 p-0">
						<?php
						if($selectMedia == "videoFile" && $videoURL ){ ?>
							<div class="singleMedia">
								<img src="<?php echo $videoImageURL ?>" class="w-100 h-100  position-relative" alt="">
								<a class="position-absolute playVideo" data-fancybox data-type="iframe" data-src="https://player.vimeo.com/video/<?php echo $videoURL ?>" href="javascript:;">
								<i class="far fa-play-circle"></i></a>
							</div> 
						<?php
						} 
						if($selectMedia == "imageFile" && $imageURL ){ ?>
							<div class="singleMedia">
								<a href="<?php echo $imageURL ?>" data-fancybox="images" data-caption="<?php echo the_title(); ?>">
									<img src="<?php echo $imageURL ?>" class="w-100 h-100 " alt="" />
								</a>
							</div> 
						<?php
						} 
						if($selectMedia == "audioFile" && $audioURL ){ ?>
							<div class="singleMedia">
								<img src="<?php echo $audioImageURL ?>" class="w-100 h-100  position-relative" alt="">
								<audio class="position-absolute" controls controlsList="nodownload"> 
									<source src="<?php echo $audioURL ?>" type="audio/mp3"> 
								</audio>
							</div>
						<?php
						} ?>
					</div>
					<div class="col-md-5">
						<div class="singleTitle">
							<h4><?php echo the_title(); ?></h4>
							<p><?php echo $termName; ?></p>
						</div>
						<div class="actionsBtn">
					<span class="btn btn-lg sFavrt-item <?php if($results){ ?> liked <?php } ?>" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php the_title(); ?>" data-post-id="<?php the_ID();?>" data-post-image="<?php if($selectMedia == "videoFile" && $videoURL ){ echo $videoImageURL; } if($selectMedia == "imageFile" && $imageURL ){ echo $imageURL; }; if($selectMedia == "audioFile" && $audioURL ){ echo $audioImageURL; } ?> "> 
						<i class="fa fa-heart"></i> 
					</span>
							<?php 
								echo do_shortcode("[wp_social_sharing social_options='facebook,twitter' twitter_username='' facebook_text='' twitter_text=''  icon_order='f,t,' show_icons='1' before_button_text='Share' text_position='' social_image='']")
							?>
						</div>
					</div>	 
				</div>
				<div class="col-12 my-5 d-flex flex-column">
					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}?>		
				</div> 
		</div> 

		<?php	// End of the loop.
		endwhile;
	}
		?>

	</main><!-- .site-main -->


</div><!-- .content-area -->
 
<?php get_footer(); ?>
