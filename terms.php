<?php
/* Template Name: terms page */ 
    get_header();
?>
<?php
		// Start the loop.
		while ( have_posts() ) :
			the_post();
			?>

			<section class = "footer_pages">
			<div class= "container">
				<div class="page_title">
					<h2><?php  	echo get_the_title();?></h2>
				</div>
				<div class="page_content">
					<p><?php the_content(); ?></p>
				</div>
			</div>
		</section>
		<?php	
		endwhile;
		?>

<?php
get_footer();
?>