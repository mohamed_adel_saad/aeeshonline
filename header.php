<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>
<?php 
$current_user_data    = wp_get_current_user();
$current_user_id      = $current_user_data->ID;
$header_enrich_status = $current_user_data->header_enrich_status;
//$he_status = get_field("header_enrich_status"); 
// var_dump($header_enrich_status);
//die();

?>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<div class="site-inner">
		<header id="masthead" class="site-header" role="banner">
			<div class="topHeader d-flex justify-content-between align-items-center">
				<span id="menu-toggle" class="menu-toggle">
					<i class="fas fa-bars"></i>
				</span> 
				<?php if ( has_nav_menu( 'secondary' ) ) : ?> 
					<div id="side-header-menu" class="side-header-menu">
						<div class="menuHeader d-flex justify-content-between align-items-center mb-3 mt-2">
							<h3 class="side-menu-title m-0"><?php echo __( 'Categories', 'twentysixteen'); ?></h3>
							<span id="menu-close" class="menu-close">
								<i class="fas fa-times"></i>
							</span>
						</div>
						
						<nav id="side-navigation" class="side-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Side Menu', 'twentysixteen' ); ?>">
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'secondary',
										'menu_class' => 'side-menu',
									)
								);
							?>
						</nav><!-- .side-navigation -->
					</div><!-- .side-header-menu -->  
				<?php endif; ?>
				<?php if(is_user_logged_in()){ 
					global $current_user; wp_get_current_user();

					if($header_enrich_status == "active"){ ?>
						<p class = "welcome"><?php echo __("welcome")?><?php echo "&nbsp;" .  $current_user->user_login ?></p>
					<?php }else{?>
							<a class="logout" href="<?php echo wp_logout_url( get_site_url() ) ?>"><?php echo __('logout')?></a>
					<?php } }?>
				<a href="<?php echo pll_get_page_url( 'home' ) ?>" class="logo">
					<img class="w-100 h-100 objectCover" src="<?php echo get_template_directory_uri() . '/img/logo.png'  ?>" alt="">
				</a>
				<span class="langSwitcher position-relative d-flex">
				<?php pll_the_languages(array('hide_current'=>1));?>
				</span>
			</div>
			<?php if(is_user_logged_in()){ ?>
				<div class="search_form">
					<?php echo get_search_form();?>
				</div>
			<?php }?>
			<div class="site-header-main "> 
				<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<div id="primary-header-menu" class="primary-header-menu">
						
						<nav id="primary-navigation" class="primary-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'primary',
										'menu_class' => 'primary-menu owl-carousel owl-theme',
										'add_li_class'  => 'item'
									)
								);
							?>
						</nav><!-- .main-navigation -->
						
					</div><!-- .site-header-menu --> 
				<?php endif; ?>  
			</div><!-- .site-header-main -->
		</header><!-- .site-header --> 

		<div id="content" class="site-content">
