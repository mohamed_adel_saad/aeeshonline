<?php
/* Template Name: subscribe page */ 
if(is_user_logged_in()){
    wp_redirect("home");
}else{
    get_header();
    
?>
<section class="subscribe">
    <div class="sub_form">
        <?php  echo do_shortcode('[user_status_form]'); ?>  
    </div>
</section>
<?php
get_footer();
    }
    
?>