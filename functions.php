<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://developer.wordpress.org/themes/advanced-topics/child-themes/
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://developer.wordpress.org/plugins/}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentysixteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * Create your own twentysixteen_setup() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 */
	function twentysixteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
		 * If you're building a theme based on Twenty Sixteen, use a find and replace
		 * to change 'twentysixteen' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'twentysixteen' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for custom logo.
		 *
		 *  @since Twenty Sixteen 1.2
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 240,
				'width'       => 240,
				'flex-height' => true,
			)
		);

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#post-thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'twentysixteen' ),
				'social'  => __( 'Social Links Menu', 'twentysixteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'script',
				'style',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://wordpress.org/support/article/post-formats/
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'status',
				'audio',
				'chat',
			)
		);

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, icons, and column width.
		 */
		add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

		// Load regular editor styles into the new block-based editor.
		add_theme_support( 'editor-styles' );

		// Load default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom color scheme.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Dark Gray', 'twentysixteen' ),
					'slug'  => 'dark-gray',
					'color' => '#1a1a1a',
				),
				array(
					'name'  => __( 'Medium Gray', 'twentysixteen' ),
					'slug'  => 'medium-gray',
					'color' => '#686868',
				),
				array(
					'name'  => __( 'Light Gray', 'twentysixteen' ),
					'slug'  => 'light-gray',
					'color' => '#e5e5e5',
				),
				array(
					'name'  => __( 'White', 'twentysixteen' ),
					'slug'  => 'white',
					'color' => '#fff',
				),
				array(
					'name'  => __( 'Blue Gray', 'twentysixteen' ),
					'slug'  => 'blue-gray',
					'color' => '#4d545c',
				),
				array(
					'name'  => __( 'Bright Blue', 'twentysixteen' ),
					'slug'  => 'bright-blue',
					'color' => '#007acc',
				),
				array(
					'name'  => __( 'Light Blue', 'twentysixteen' ),
					'slug'  => 'light-blue',
					'color' => '#9adffd',
				),
				array(
					'name'  => __( 'Dark Brown', 'twentysixteen' ),
					'slug'  => 'dark-brown',
					'color' => '#402b30',
				),
				array(
					'name'  => __( 'Medium Brown', 'twentysixteen' ),
					'slug'  => 'medium-brown',
					'color' => '#774e24',
				),
				array(
					'name'  => __( 'Dark Red', 'twentysixteen' ),
					'slug'  => 'dark-red',
					'color' => '#640c1f',
				),
				array(
					'name'  => __( 'Bright Red', 'twentysixteen' ),
					'slug'  => 'bright-red',
					'color' => '#ff675f',
				),
				array(
					'name'  => __( 'Yellow', 'twentysixteen' ),
					'slug'  => 'yellow',
					'color' => '#ffef8e',
				),
			)
		);

		// Indicate widget sidebars can use selective refresh in the Customizer.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'twentysixteen_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'twentysixteen_content_width', 840 );
}
add_action( 'after_setup_theme', 'twentysixteen_content_width', 0 );

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Sixteen 1.6
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentysixteen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentysixteen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentysixteen_resource_hints', 10, 2 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Sidebar', 'twentysixteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 1', 'twentysixteen' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Content Bottom 2', 'twentysixteen' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'twentysixteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );

if ( ! function_exists( 'twentysixteen_fonts_url' ) ) :
	/**
	 * Register Google fonts for Twenty Sixteen.
	 *
	 * Create your own twentysixteen_fonts_url() function to override in a child theme.
	 *
	 * @since Twenty Sixteen 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function twentysixteen_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		/*
		 * translators: If there are characters in your language that are not supported
		 * by Merriweather, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
		}

		/*
		 * translators: If there are characters in your language that are not supported
		 * by Montserrat, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Montserrat:400,700';
		}

		/*
		 * translators: If there are characters in your language that are not supported
		 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentysixteen' ) ) {
			$fonts[] = 'Inconsolata:400';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg(
				array(
					'family'  => urlencode( implode( '|', $fonts ) ),
					'subset'  => urlencode( $subsets ),
					'display' => urlencode( 'fallback' ),
				),
				'https://fonts.googleapis.com/css'
			);
		}

		return $fonts_url;
	}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentysixteen_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Add Bootstrap.
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );

	// Add Slick.
	wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/css/slick.css' );

	// Add Slick Theme.
	wp_enqueue_style( 'slick-theme-style', get_template_directory_uri() . '/css/slick-theme.css' );

	// Add FancyBox.
	wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/css/jquery.fancybox.min.css' );

	// Theme stylesheet.
	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri(), array(), '20190507' );

	// Add FontAwesome.
	wp_enqueue_style( 'fontawesome-style', get_template_directory_uri() . '/css/all.css' );
	
	// Add Owl Carousel Style.
	wp_enqueue_style( 'owl-style', get_template_directory_uri() . '/css/owl.carousel.min.css' );
	
	// Add Owl Carousel Style.
	wp_enqueue_style( 'owl-default-style', get_template_directory_uri() . '/css/owl.theme.default.min.css' );
	
	//  slider-style.
	wp_enqueue_style( 'slider-style', get_template_directory_uri() . '/css/jquery.hislide.min.css' );
	
	// Add mohamed style
	wp_enqueue_style( 'm-style-style', get_template_directory_uri() . '/mohamed_style.css' );

	// Theme block stylesheet.
	wp_enqueue_style( 'twentysixteen-block-style', get_template_directory_uri() . '/css/blocks.css', array( 'twentysixteen-style' ), '20190102' );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20170530' );
	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20170530' );
	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20170530' );
	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20170530', true );

	// Add fancybox Script.
	wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/jquery.fancybox.min.js', array( 'jquery' ), true );

	// Add Materialize Script.
	wp_enqueue_script( 'matrilize-script', get_template_directory_uri() . '/js/jquery.hislide.min.js', array( 'jquery' ), true );

	


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20170530' );
	}

	wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), true );
	
	wp_enqueue_script( 'home_carsouel', get_template_directory_uri() . '/js/home-carsouel.js', array( 'jquery' ), true );

	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20181217', true );
	
	wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), true );

	wp_enqueue_script( 'owl-carousel-script', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), true );

	wp_enqueue_script( 'fontAwesome-script', get_template_directory_uri() . '/js/all.js', array( 'jquery' ), true );

	wp_localize_script(
		'twentysixteen-script',
		'screenReaderText',
		array(
			'expand'   => __( 'expand child menu', 'twentysixteen' ),
			'collapse' => __( 'collapse child menu', 'twentysixteen' ),
		)
	);
	wp_localize_script(
		'twentysixteen-script',
		'vl_ajax_script',
		array(
			'ajaxurl'  => admin_url( 'admin-ajax.php' ),
			'site_url' => site_url(),
		)
	);
}
add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Enqueue styles for the block-based editor.
 *
 * @since Twenty Sixteen 1.6
 */
function twentysixteen_block_editor_styles() {
	// Block styles.
	wp_enqueue_style( 'twentysixteen-block-editor-style', get_template_directory_uri() . '/css/editor-blocks.css', array(), '20190102' );
	// Add custom fonts.
	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );
}
add_action( 'enqueue_block_editor_assets', 'twentysixteen_block_editor_styles' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'twentysixteen_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ) . substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ) . substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ) . substr( $color, 2, 1 ) );
	} elseif ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array(
		'red'   => $r,
		'green' => $g,
		'blue'  => $b,
	);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 840 <= $width ) {
		$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
	}

	if ( 'page' === get_post_type() ) {
		if ( 840 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	} else {
		if ( 840 > $width && 600 <= $width ) {
			$sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		} elseif ( 600 > $width ) {
			$sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10, 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentysixteen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		if ( is_active_sidebar( 'sidebar-1' ) ) {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		} else {
			$attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
		}
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentysixteen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args' );

add_filter('show_admin_bar', '__return_false');

function customAdmin() {
    $url = get_settings('siteurl');
    $url = $url . '/wp-content/themes/twentysixteen/admin.css';
    echo '<!-- custom admin css -->
          <link rel="stylesheet" type="text/css" href="' . $url . '" />
          <!-- /end custom adming css -->';
}
add_action('admin_head', 'customAdmin');

/*
Plugin Name: Force Post Title
Plugin URI: http://appinstore.com
Description: Forces user to assign a Title to a post before publishing 
Author: Jatinder Pal Singh
Version: 0.1
Author URI: http://appinstore.com/
*/ 
function force_post_title_init() {
  wp_enqueue_script('jquery');
}

function force_post_title() {
  echo "<script type='text/javascript'>\n";
  echo "
  jQuery('#publish').click(function(){
        var testervar = jQuery('[id^=\"titlediv\"]')
        .find('#title');
        if (testervar.val().length < 1)
        {
            setTimeout(\"jQuery('#ajax-loading').css('visibility', 'hidden');\", 100);
			// alert('POST TITLE is required');
			jQuery('[id^=\"titlediv\"]').append('<p>TITLE IS REQUIRED</p>');
            setTimeout(\"jQuery('#publish').removeClass('button-primary-disabled');\", 100);
            return false;
        }
    });
  ";
   echo "</script>\n";
}
add_action('admin_init', 'force_post_title_init');
add_action('edit_form_advanced', 'force_post_title');
// Add this row below to get the same functionality for page creations.
add_action('edit_page_form', 'force_post_title');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'twentysixteen'),
    'secondary' => __( 'Side Menu', 'twentysixteen' ),
 ) );



 function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);



//-------------------------------------------------------

function homeLayout ($postTypeName, $sectionTitle, $firstTab, $secondTab, $thirdTab, $taxonomySlug, $sectionBg, $modal) {
	$videoContent   = false;
    $audioContent   = false;
    $imageContent   = false;
    $loop = new WP_Query( array( 
		'post_type' => $postTypeName, 
		'posts_per_page' => -1, 
		'orderby' => 'menu_order'
		)); 
    $counter = 0;
    while ( $loop->have_posts() ) : $loop->the_post(); 
        $counter++; 
        $selectMedia= get_field('select_media');
        $videoURL   = get_field('post_video');
        $audioURL   = get_field('post_audio');
        $imageURL   = get_field('featured_image');

        if($selectMedia == "videoFile"){ 
            $videoContent = true;
        } 
        if($selectMedia == "audioFile"){ 
            $audioContent = true;
        } 
        if($selectMedia == "imageFile"){ 
            $imageContent = true;
        } 
    endwhile; wp_reset_query(); 
	?>
	<section class="<?php echo $sectionBg ?>">
		<div class="container">
			<div class="row">
				<div id="tabs" class="col-12 py-4 mt-2">
					<div class="mainTitle">
						<h2><?php echo __( $sectionTitle ) ?></h2>
					</div>
					<ul class="nav tablist" > 
						<?php 
						if($videoContent){ ?>
							<li class="nav-item ">
								<a href="#<?php echo $firstTab; ?>" id="videoTab" class="nav-link" role="tab" data-toggle="tab"> 
									<h2><?php echo __( 'Video' ) ?></h2> 
								</a> 
							</li>
						<?php
						} 
						if($audioContent){ ?>
							<li class="nav-item ">
								<a href="#<?php echo $secondTab; ?>" id="audioTab" class="nav-link" role="tab" data-toggle="tab"> 
									<h2><?php echo __( 'Audio' ) ?></h2> 
								</a> 
							</li> 
						<?php   
						} 
						if($imageContent){ ?>
							<li class="nav-item ">
								<a href="#<?php echo $thirdTab; ?>" id="imageTab" class="nav-link" role="tab" data-toggle="tab">
									<h2><?php echo __( 'Photo' ) ?></h2> 
								</a>
							</li>
						<?php
						}
						?> 
						<span></span>
					</ul>
					<div class="tab-content"> 
					<a href="<?php echo get_post_type_archive_link( $postTypeName ) . '?/video' ?>" class="moreBtn"><?php echo __('More') ?></a> 
						<?php
						if($videoContent){ ?> 
							<div id="<?php echo $firstTab; ?>" role="tabpanel" class="tab-pane slickSlider videoContent">

								<?php 
									$loop = new WP_Query( array( 
										'post_type' => $postTypeName, 
										'posts_per_page' => -1, 
										'orderby' => 'menu_order'
										)); 
									while ( $loop->have_posts() ) : $loop->the_post(); 
										global $wpdb; 
										$counter++;
										$postLink  	   = get_post_permalink();
										$theTitle      = get_the_title();
										$theContent    = get_the_content();    
										$videoURL      = get_field('post_video'); 
										$videoImageURL = get_field('featured_video_image');
										$terms		   = get_the_terms($loop->ID, $taxonomySlug);
										if (is_array($terms) || is_object($terms)){
											foreach($terms as $term) { 
												$term_id   = $term->term_id; 
												$child_term = get_term( $term_id, $taxonomySlug );
												$parent_term = get_term( $child_term->parent, $taxonomySlug );
												$child_term_name = $child_term->name;
												$parent_term_name = $parent_term->name;
											}; 
										};
										$postID   		= get_the_ID(); 
										$currentUserId	= get_current_user_id();
										$queryDB 	= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
										$results 	= $wpdb->get_results($queryDB);
								?>
								<?php 
									if($videoURL){ ?> 
								<div class="contentData <?php echo $modal; ?>">
									<div class="position-relative imageHover">
										<img src="<?php echo $videoImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
										<div class="hoverItem w-100">
											<div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
												<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
													<i aria-hidden="true" class="fa fa-play"></i>
												</a> 
												<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $videoImageURL; ?> ">
													<i aria-hidden="true" class="fa fa-heart"></i>
												</a>  
											</div> 
										</div> 
									</div>
									<div class="itemDetails text-break">
										<a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
										<?php 
											if($parent_term = NULL ){ ?>
												<p><?php echo $parent_term_name ?></p>
											<?php
											} else{ ?>
												<p><?php echo $child_term_name ?></p>
											<?php
											}
										?>
									</div>
								</div>
								<?php   } 
								?>
								<?php 
									endwhile;  wp_reset_query(); 
								?>
							</div>
						<?php 
						}
						if($audioContent){  ?>
							<div id="<?php echo $secondTab; ?>" role="tabpanel" class="tab-pane slickSlider audioContent">
								<?php 
									$loop = new WP_Query( array( 
										'post_type' => $postTypeName, 
										'posts_per_page' => -1, 
										'orderby' => 'menu_order'
										)); 
									while ( $loop->have_posts() ) : $loop->the_post(); 
										global $wpdb; 
										$counter++;
										$postLink  	= get_post_permalink();
										$theTitle   = get_the_title();
										$theContent = get_the_content();
										$audioURL   = get_field('post_audio'); 
										$audioImageURL = get_field('featured_audio_image');
										$terms   	= get_the_terms($loop->ID, $taxonomySlug);
										if (is_array($terms) || is_object($terms)){
											foreach($terms as $term) { 
												$term_id = $term->term_id; 
												$child_term = get_term( $term_id, $taxonomySlug );
												$parent_term = get_term( $child_term->parent, $taxonomySlug );
												$child_term_name = $child_term->name;
												$parent_term_name = $parent_term->name;
											}; 
										};
										$postID   		= get_the_ID(); 
										$currentUserId	= get_current_user_id();
										$queryDB 	= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
										$results 	= $wpdb->get_results($queryDB); 
								?>
								<?php 
									if($audioURL){ ?> 
								<div class="contentData <?php echo $modal; ?>">
									<div class="position-relative imageHover">
										<img src="<?php echo $audioImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
										<div class="hoverItem w-100">
											<div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
												<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
													<i aria-hidden="true" class="fa fa-play"></i>
												</a> 
												<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $audioImageURL; ?> ">
													<i aria-hidden="true" class="fa fa-heart"></i>
												</a>  
											</div> 
										</div> 
									</div>
									<div class="itemDetails text-break">
										<a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
										<?php 
											if($parent_term = NULL ){ ?>
												<p><?php echo $parent_term_name ?></p>
											<?php
											} else{ ?>
												<p><?php echo $child_term_name ?></p>
											<?php
											}
										?>
									</div>
								</div>
								<?php } 
								?>
								<?php 
									endwhile;  wp_reset_query(); 
								?>
							</div>
						<?php   
						} 
						if($imageContent){  ?>
							<div id="<?php echo $thirdTab; ?>" role="tabpanel" class="tab-pane slickSlider imageContent "> 
								<?php 
									$loop = new WP_Query( array( 
										'post_type' => $postTypeName, 
										'posts_per_page' => -1, 
										'orderby' => 'menu_order' 
										)); 
									while ( $loop->have_posts() ) : $loop->the_post();
										global $wpdb;  
										$counter++;
										$postLink  	= get_post_permalink();
										$theTitle   = get_the_title();
										$theContent = get_the_content();
										$imageURL   = get_field('featured_image');
										$terms   	= get_the_terms($loop->ID, $taxonomySlug);
										if (is_array($terms) || is_object($terms)){
											foreach($terms as $term) { 
												$term_id = $term->term_id; 
												$child_term = get_term( $term_id, $taxonomySlug );
												$parent_term = get_term( $child_term->parent, $taxonomySlug );
												$child_term_name = $child_term->name;
												$parent_term_name = $parent_term->name;
												$child_term_link  = get_term_link( $term_id, $taxonomySlug );
												$parent_term_link  = get_term_link( $child_term->parent, $taxonomySlug );
											}; 
										};
										$postID   		= get_the_ID(); 
										$currentUserId	= get_current_user_id();
										$queryDB 	= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
										$results 	= $wpdb->get_results($queryDB);
								?>
								<?php 
									if($imageURL){ ?>
								<div class="contentData <?php echo $modal; ?>">
									<div class="position-relative imageHover">
										<img src="<?php echo $imageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
										<div class="hoverItem w-100">
											<div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
												<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
													<i aria-hidden="true" class="fa fa-eye"></i>
												</a> 
												<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $imageURL; ?> ">
													<i aria-hidden="true" class="fa fa-heart"></i>
												</a>  
											</div> 
										</div> 
									</div>
									<div class="itemDetails text-break">
										<a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
										<?php 
											if($parent_term = NULL ){ ?>
												
													<p><a href="<?php echo $parent_term_link ?>"></a><?php echo $parent_term_name ?></p>
											<?php
											} else{ ?>
												
													<p><a href="<?php echo $child_term_link ?>"></a><?php echo $child_term_name ?></p> 
											<?php
											}
										?>
									</div>
								</div>
									<?php } 
								?>
								<?php 
									endwhile;  wp_reset_query(); 
								?> 
							</div>  
						<?php
						}
						?> 
					</div> 
				</div> 
			</div>
		</div>
	</section>
	<?php
}


// add_filter( 'the_title', 'my_updated_article_title', 10 );
// function my_updated_article_title($title) {
// 	return html_entity_decode($title);
// }


//  // Function to create extra table in DB
// add_action("after_switch_theme", "create_extra_table");

// function create_extra_table(){
//     global $wpdb;

//     require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

//     $table_name = $wpdb->prefix . "user_favorite_posts";  //get the database table prefix to create my new table

//     $sql = "CREATE TABLE $table_name (
//       id int(10) unsigned NOT NULL AUTO_INCREMENT,
//       fav_user_id varchar(255) NOT NULL,
//       fav_post_id varchar(255) NOT NULL,
//       fav_post_url varchar(255) NOT NULL,
//       fav_post_title varchar(255) NOT NULL,
//       fav_post_image varchar(255) NOT NULL, 
//       -- lang varchar(5) NOT NULL,
//       -- notes varchar(255) DEFAULT NULL,
//       PRIMARY KEY  (id)
//     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

//     dbDelta( $sql );
// }


add_action( 'wp_ajax_add_post_to_list_ajax_hook', 'addPostsToMyList' );
function addPostsToMyList() {
	global $wpdb;
	$response_data 	= array(); 
	$table_name 	='user_favorite_posts';
	$userID 		= $_POST['userID'];
	$postID 		= $_POST['postID'];
	$postLink 		= $_POST['postLink'];
	$postTitle 		= $_POST['postTitle'];
	$postImage 		= $_POST['postImage'];
	$selectFromDB 	= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $userID  . ' AND fav_post_id = "' . $postID . '"' ;
	$selectedResult = $wpdb->get_results($selectFromDB);
	
	if($selectedResult){
		$response_data['check_insert_status'] = false;
	} else {
		$success = $wpdb->insert("wp_user_favorite_posts", array(
			"fav_user_id"	=> $userID,
			"fav_post_id"	=> $postID,
			"fav_post_url"	=> $postLink,
			"fav_post_title"=> $postTitle,
			"fav_post_image"=> $postImage,
		));
		$response_data['check_insert_status'] = true;
		$response_data['successInsert'] = $success;
		$response_data['userID'] = $userID;
		$response_data['postLink'] = $postLink;
		$response_data['postTitle'] = $postTitle;
		$response_data['postID'] = $postID;
		$response_data['postImage'] = $postImage; 
	}
	echo json_encode($response_data);
	die;
}


add_action( 'wp_ajax_delete_post_from_list_ajax_hook', 'deletePostFromMyList' );
function deletePostFromMyList() {
	global $wpdb;
	$response_data 		= array(); 
	$table_name 		='user_favorite_posts';
	$postID 			= $_POST['postID'];
	$userID 			= $_POST['userID']; 
	$checkUserLikes 	= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $userID  ;
	$checkUserLikesInDB = $wpdb->get_results($checkUserLikes);
		
	$success = $wpdb->delete( 'wp_user_favorite_posts', array(
		"fav_user_id" => $userID,
		"fav_post_id" => $postID,
	));

	if($success){ 
		$response_data['deleteResult'] = true;
	} else { 
		$response_data['deleteResult'] = false;
	}

	echo json_encode($response_data);
	die;
}



/**
 * Adding extra fields to the user profile, storing the subscription status of user number.
 *
 * @package VLDCP
 */

if ( ! function_exists( 'vldcp_user_meta_fields' ) || ! function_exists( 'save_vldcp_user_meta_fields' ) ) {

	add_action( 'show_user_profile', 'vldcp_user_meta_fields' );
	add_action( 'edit_user_profile', 'vldcp_user_meta_fields' );

	/**
	 * Creates a meta fields in the user profile.
	 *
	 * @param string $user user object
	 * @package VLDCP
	 */
	function vldcp_user_meta_fields( $user ) {
		?>

<table class="form-table">
	<tr>
		<th><label for="header_enrich_status">Header Enrich Status</label></th>
		<td>
			<input type="text" name="header_enrich_status" id="header_enrich_status"
				value="<?php echo esc_attr( get_the_author_meta( 'header_enrich_status', $user->ID ) ); ?>"
				class="regular-text" /><br />
		</td>
	</tr>
</table>
		<?php
	}


	add_action( 'personal_options_update', 'save_vldcp_user_meta_fields' );
	add_action( 'edit_user_profile_update', 'save_vldcp_user_meta_fields' );

	/**
	 * Handling the saving action of user meta fields.
	 *
	 * @param string $user_id
	 * @package VLDCP
	 */
	function save_vldcp_user_meta_fields( $user_id ) {
		if ( ! current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}
		update_user_meta( $user_id, 'header_enrich_status', $_POST['header_enrich_status'] );
	}

} 

/**
 * This function returns a page permalink
 * for the current website language.
 *
 * @author  Mauricio Gelves <mg@maugelves.com>
 * @param   $page_slug      string          WordPress page slug
 * @return                  string|false    Page Permalink or false if the page is not found
 */
if ( ! function_exists( 'pll_get_page_url' ) ) {
	function pll_get_page_url( $page_slug ) {
		// Check parameter
		if ( empty( $page_slug ) ) {
			return false;
		}

		// Get the page
		$page = get_page_by_path( $page_slug );
		// Check if the page exists
		if ( empty( $page ) || is_null( $page ) ) {
			return false;
		}
		// Get the URL
		$page_id_current_lang = pll_get_post( $page->ID );
		// Return the current language permalink
		return empty( $page_id_current_lang ) ? get_permalink( $page->ID ) : get_permalink( $page_id_current_lang );
	}
}


