<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-12"> 
			<section id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
					<header class="page-header mainTitle pt-3">
						<h1 class="page-title"><?php echo __( 'Search' )?></h1>
					</header><!-- .page-header -->
				<?php if ( have_posts() ) : ?> 
					
	<div id="11" role="tabpanel" class="tab-pane userList videoContent d-flex justify-content-center flex-wrap">
					<?php
					// Start the loop.
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content', 'search' );

						// End the loop.
					endwhile; ?>

	</div> 
	<?php
					// Previous/next page navigation.
					// the_posts_pagination(
					// 	array(
					// 		'prev_text'          => __( 'Previous page', 'twentysixteen' ),
					// 		'next_text'          => __( 'Next page', 'twentysixteen' ),
					// 		'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
					// 	)
					// );

					// If no content, include the "No posts found" template.
				else :
?>
					<div class="d-flex justify-content-center py-3">
						<h6><?php echo __('No results found, please try again.') ?></h6>
					</div>
<?php
				endif;
				?>

				</main><!-- .site-main -->
			</section><!-- .content-area -->
		</div>
	</div>
</div>
<?php get_footer(); ?>
