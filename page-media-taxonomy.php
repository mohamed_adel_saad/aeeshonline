<?php
/* Template Name: media taxonomy */ 
    get_header();
    $siteLanguage 	= get_locale();
    $RTL 			= $siteLanguage == "ar" ? true : false ;
?>

<?php

if (isset($_GET['tname'])) {
    $tax_term = $_GET['tname'];
    $tax_name = $_GET['xname'];
    $cpt = $_GET['cpt'];
    $media_type = $_GET['mediaType'];
    // echo $tax_name;
    // echo $tax_term;
    // echo $cpt ;
    // echo $media_type;
  } else {
    //Handle the case where there is no parameter
    // echo "missing parameter";
  }



?>
<?php
$args = array(
    'post_type'             => $cpt,
    'posts_per_page'        => -1, 
    'post_status'           => 'publish',
    'tax_query'             => array(
                                array(
                                    'taxonomy' => $tax_name,
                                    'field'    => 'slug',
                                    'terms'    => $tax_term,
                                ),
                            ),
);
$_posts = new WP_Query( $args );
?>
<div class="container">
    <div class="row">
    <?php if ( ! is_user_logged_in() ) { ?>
			<div class="container">
				<div class="row py-5">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					<?php if($RTL){ ?>
						<span class="alert alert-danger text-right w-100 float-right">
							يجب
							<a class='alert-link' 
								href="<?php  echo pll_get_page_url('/subscribe') ?>">
								الاشتراك او تسجيل الدخول
							</a>
							لمشاهدة المحتوى
						</span>
					<?php } else{ ?>
						<span class="alert alert-danger text-left w-100 float-left">
							You should
							<a class='alert-link' 
								href="<?php echo pll_get_page_url('/subscribe') ?>">
								login or subscribe
							</a>
							to view content
						</span>
					<?php } ?>
                    </div>
				</div>
			</div>
				<?php
			} else { ?>
        <?php

    if( $_posts->have_posts() ) :
        while( $_posts->have_posts() ) : $_posts->the_post();
        global $wpdb; 
        $postLink  	= get_post_permalink();
        $selectMedia   = get_field('select_media');
        $theTitle      = get_the_title();
        $theContent    = get_the_content();    
        $videoURL      = get_field('post_video'); 
        $videoImageURL = get_field('featured_video_image');
        $selectMedia   = get_field('select_media');
        $postID   		= get_the_ID();  
        $currentUserId	= get_current_user_id();
        $queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
        $results 		= $wpdb->get_results($queryDB);

            if($media_type == "video" && $selectMedia == "videoFile"){ ?> 
            
                <div class="media_tax_data col-6 col-md-4 col-lg-3 mt-4">
                    <div class="position-relative imageHover">
                        <img src="<?php echo $videoImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
                        <div class="hoverItem w-100">
                            <div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
                                <a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
                                    <i aria-hidden="true" class="fa fa-play"></i>
                                </a> 
                                <a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $videoImageURL; ?> ">
                                    <i aria-hidden="true" class="fa fa-heart"></i>
                                </a>  
                            </div> 
                        </div> 
                    </div>
                    <div class="itemDetails text-break">
                        <a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
                    </div>
                </div>
            <?php   } 
    endwhile;
    endif;
    wp_reset_postdata();  ?> 


    <?php
    if( $_posts->have_posts() ) :
        while( $_posts->have_posts() ) : $_posts->the_post(); 
            global $wpdb;
            $postLink  	= get_post_permalink();
            $theTitle   = get_the_title();
            $theContent = get_the_content();
            $imageURL   = get_field('featured_image');
            $selectMedia   = get_field('select_media');
            $postID   		= get_the_ID();  
            $currentUserId	= get_current_user_id();
            $queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
            $results 		= $wpdb->get_results($queryDB);
            ?>

            <?php  if($media_type == "image" && $selectMedia == "imageFile"){ ?>
                <div class="media_tax_data col-6 col-md-4 col-lg-3 mt-4">
                    <div class="position-relative imageHover">
                        <img src="<?php echo $imageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
                        <div class="hoverItem w-100">
                            <div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
                                <a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
                                    <i aria-hidden="true" class="fa fa-eye"></i>
                                </a> 
                                <a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $imageURL; ?> ">
                                    <i aria-hidden="true" class="fa fa-heart"></i>
                                </a> 
                            </div> 
                        </div> 
                    </div>
                    <div class="itemDetails text-break">
                        <a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
                    </div>
                </div>
            <?php }  ?>
        <?php endwhile;
    endif;
    wp_reset_postdata(); ?> 

    
    <?php
    if( $_posts->have_posts() ) :
        while( $_posts->have_posts() ) : $_posts->the_post(); 
        global $wpdb;
        $postLink  	= get_post_permalink();
        $selectMedia   = get_field('select_media');
        $theTitle      = get_the_title();
        $theContent    = get_the_content();    
        $audioURL   = get_field('post_audio'); 
        $audioImageURL = get_field('featured_audio_image');
        $postID   		= get_the_ID();  
        $currentUserId	= get_current_user_id();
        $queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
        $results 		= $wpdb->get_results($queryDB);
            ?>
            <?php  if($media_type == "audio" && $selectMedia == "audioFile"){ ?>
                <div class="media_tax_data col-6 col-md-4 col-lg-3 mt-4">
                    <div class="position-relative imageHover">
                        <img src="<?php echo $audioImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
                        <div class="hoverItem w-100">
                            <div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
                                <a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
                                    <i aria-hidden="true" class="fa fa-play"></i>
                                </a> 
                                <a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $audioImageURL; ?> ">
                                    <i aria-hidden="true" class="fa fa-heart"></i>
                                </a>  
                            </div> 
                        </div> 
                    </div>
                    <div class="itemDetails text-break">
                        <a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
                    </div>
                </div>
            <?php }  ?>
        <?php endwhile;
    endif;
    wp_reset_postdata(); 
    
    }?> 

    </div>
</div>                                                       

<?php
    get_footer();

?>