<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<?php $site_lang = get_locale();?>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info text-center">
				<div class="brand_icon">
					<span>Powered by: 
						<img alt="" src="<?php echo get_template_directory_uri() . '/img/orange-logo.png' ?>" width="45" height="45">
					</span>
				</div>
				<ul>
					<li>
						<a href="<?php echo pll_get_page_url( '/home' ) ?>">
							<?php echo __('Home')?>
						</a>
					</li>
					<li>
						<a href="<?php echo pll_get_page_url( '/about' ) ?>">
							<?php echo __('About us')?>
						</a>
					</li>
					<li>
						<a href="<?php echo pll_get_page_url( '/faq' ) ?>">
							<?php echo __('FAQ')?>
						</a>
					</li>
					<li>
						<a href="<?php echo pll_get_page_url( '/terms-of-use' ) ?>">
							<?php echo __('Terms &amp; Conditions')?>
						</a>
					</li>
					<li>
						<a href="<?php echo pll_get_page_url( '/privacy' ) ?>">
							<?php echo __('Privacy Policy')?>
						</a>
					</li>
				</ul>
				<div class="copy_rights">
					<p>
						<?php echo __('All copyrights reserved')?>&copy;
							<span> 3eeshonline.com  </span>
						 <?php echo __('Copyright')?> &copy; <?php echo date("Y"); ?>
					</p>
				</div>  
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->
<?php wp_footer(); ?>
</body>
</html>


