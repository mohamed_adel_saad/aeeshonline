<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */


get_header();
global $wpdb;
$siteLanguage 	= get_locale();
$RTL 			= $siteLanguage == "ar" ? true : false ;  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if ( ! is_user_logged_in() ) { ?>
			<div class="container">
				<div class="row py-5">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12">
					<?php if($RTL){ ?>
						<span class="alert alert-danger text-right w-100 float-right">
							يجب
							<a class='alert-link' 
								href="<?php echo pll_get_page_url('/subscribe') ?>">
								الاشتراك او تسجيل الدخول
							</a>
							لمشاهدة المحتوى
						</span>
					<?php } else{ ?>
						<span class="alert alert-danger text-left w-100 float-left">
							You should
							<a class='alert-link' 
								href="<?php echo pll_get_page_url('/subscribe') ?>">
								login or subscribe
							</a>
							to view content
						</span>
					<?php } ?>
                    </div>
				</div>
			</div>
				<?php
			} else {
			?>

<?php   
	 
	$taxonomyName;
	$counter = 0;
	$firstTab = "firstTab";
	$secondTab = "secondTab";
	$thirdTab = "thirdTab";
	$videoContent   = false;
    $audioContent   = false;
	$imageContent   = false;
	$parent_terms = get_terms($taxonomyName, array('parent' => 0, 'orderby' => 'slug', 'hide_empty' => false));   
	$post_type = get_queried_object();
	$taxonomies = get_taxonomies( [ 'object_type' => [ $post_type->rewrite['slug'] ] ] );

	foreach($taxonomies as $taxonomie) {  
		$taxonomyName = $taxonomie;
	}
	$custom_terms = get_terms($taxonomyName , array('parent' => 0, 'orderby' => 'slug', 'hide_empty' => false));
	
	foreach($custom_terms as $custom_term) { 
		$counter++;
		$firstTabContent = $firstTab . $counter;
		$secondTabContent = $secondTab . $counter;
		$thirdTabContent = $thirdTab . $counter;
		
		$args = array(
			'post_type'             => $post_type->name,
			'posts_per_page'        => -1, 
			'post_status'           => 'publish',
			'tax_query'             => array(
										array(
											'taxonomy' => $custom_term->taxonomy,
											'field'    => 'slug',
											'terms'    => $custom_term->slug,
										),
									),
		);
		$_posts = new WP_Query( $args );
	
		if( $_posts->have_posts() ) :
			while( $_posts->have_posts() ) : $_posts->the_post(); 
				$selectMedia   = get_field('select_media');
				$audioURL   = get_field('post_audio'); 
				$videoURL      = get_field('post_video');
				$imageURL   = get_field('featured_image');
	
				if($selectMedia == "videoFile" && $videoURL){ 
					$videoContent = true;
				} 
				if($selectMedia == "audioFile" && $audioURL){ 
					$audioContent = true;
				}  
				if($selectMedia == "imageFile" && $imageURL){ 
					$imageContent = true;
				}  
	
				// End the loop.
			endwhile; wp_reset_postdata(); 
		endif; 

		?>
		<section class="panel">
			<div class="container">
				<div class="row">
					<div id="tabs" class="col-12 py-4 mt-2">
						<div class="mainTitle">
							<h2><?php echo  $custom_term->name ?></h2>
						</div>
						<ul class="nav tablist" > 
							<?php 
							if($videoContent){   ?>
								<li class="nav-item ">
									<a href="#<?php echo $firstTabContent ; ?>" id="videoTab" class="nav-link" role="tab" data-toggle="tab"> 
										<h2><?php echo __( 'Video' ) ?></h2> 
									</a> 
								</li>
							<?php
							} 
							if($audioContent){ ?>
								<li class="nav-item ">
									<a href="#<?php echo $secondTabContent ; ?>" id="audioTab" class="nav-link" role="tab" data-toggle="tab"> 
										<h2><?php echo __( 'Audio' ) ?></h2> 
									</a> 
								</li> 
							<?php   
							} 
							if($imageContent){ ?>
								<li class="nav-item ">
									<a href="#<?php echo $thirdTabContent ; ?>" id="imageTab" class="nav-link" role="tab" data-toggle="tab">
										<h2><?php echo __( 'Photo' ) ?></h2> 
									</a>
								</li>
							<?php
							}
							?> 
							<span></span>
						</ul>
						<div class="tab-content"> 
							
							<?php
							if($videoContent){ ?> 
								<div id="<?php echo $firstTabContent ; ?>" role="tabpanel" class="tab-pane videoContent">
								<?php 
								if($RTL){ ?>
									<a href="<?php echo get_site_url(). '/ميديا?mediaType=video&cpt=' . $post_type->name . '&tname=' .  $custom_term->slug . '&xname=' . $custom_term->taxonomy ?>" class="moreBtn"><?php echo 'المزيد' ?></a> 
								<?php
								}else{ ?>
									<a href="<?php echo get_site_url(). '/media-taxonomy?mediaType=video&cpt=' . $post_type->name . '&tname=' .  $custom_term->slug . '&xname=' . $custom_term->taxonomy ?>" class="moreBtn"><?php echo 'More' ?></a> 
								<?php
								}?> 
								<div class="slickSlider ">
									<?php
									$args = array(
										'post_type'             => $post_type->name,
										'posts_per_page'        => -1, 
										'post_status'           => 'publish',
										'tax_query'             => array(
																	array(
																		'taxonomy' => $custom_term->taxonomy,
																		'field'    => 'slug',
																		'terms'    => $custom_term->slug,
																	),
																),
									);
									$_posts = new WP_Query( $args );

									if( $_posts->have_posts() ) :
										while( $_posts->have_posts() ) : $_posts->the_post(); 
											$postLink  	= get_post_permalink();
											$selectMedia   = get_field('select_media');
											$theTitle      = get_the_title();
											$theContent    = get_the_content();    
											$videoURL      = get_field('post_video'); 
											$videoImageURL = get_field('featured_video_image');
											global $wpdb; 
											$postID   		= get_the_ID();  
											$currentUserId	= get_current_user_id();
											$queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
											$results 		= $wpdb->get_results($queryDB);
											if($videoURL && $selectMedia == "videoFile"){ ?> 
												<div class="contentData">
													<div class="position-relative imageHover">
														<img src="<?php echo $videoImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
														<div class="hoverItem w-100">
															<div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
																<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
																	<i aria-hidden="true" class="fa fa-play"></i>
																</a> 
																<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $videoImageURL; ?> ">
																	<i aria-hidden="true" class="fa fa-heart"></i>
																</a>  
															</div> 
														</div> 
													</div>
													<div class="itemDetails text-break">
														<a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
													</div>
												</div>
											<?php   } 
											?>
							

										<?php endwhile;
									endif;
									wp_reset_postdata(); ?>
								</div>
								</div>
							<?php } 
							if($audioContent){ ?> 
								<div id="<?php echo $secondTabContent ; ?>" role="tabpanel" class="tab-pane audioContent">
								<?php 
								if($RTL){ ?>
									<a href="<?php echo get_site_url(). '/ميديا?mediaType=audio&cpt=' . $post_type->name . '&tname=' .  $custom_term->slug . '&xname=' . $custom_term->taxonomy ?>" class="moreBtn"><?php echo 'المزيد' ?></a>
								<?php
								} else{?>
									<a href="<?php echo get_site_url(). '/media-taxonomy?mediaType=audio&cpt=' . $post_type->name . '&tname=' .  $custom_term->slug . '&xname=' . $custom_term->taxonomy ?>" class="moreBtn"><?php echo 'More' ?></a>
								<?php
								}?>

								<div class="slickSlider">		 
									<?php
									$args = array(
										'post_type'             => $post_type->name,
										'posts_per_page'        => -1, 
										'post_status'           => 'publish',
										'tax_query'             => array(
																	array(
																		'taxonomy' => $custom_term->taxonomy,
																		'field'    => 'slug',
																		'terms'    => $custom_term->slug,
																	),
																),
									);
									$_posts = new WP_Query( $args );

									if( $_posts->have_posts() ) :
										while( $_posts->have_posts() ) : $_posts->the_post(); 
											$postLink  	= get_post_permalink();
											$selectMedia   = get_field('select_media');
											$theTitle      = get_the_title();
											$theContent    = get_the_content();    
											$audioURL   = get_field('post_audio'); 
											$audioImageURL = get_field('featured_audio_image');
											global $wpdb; 
											$postID   		= get_the_ID();  
											$currentUserId	= get_current_user_id();
											$queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
											$results 		= $wpdb->get_results($queryDB);
											if($audioURL && $selectMedia == "audioFile"){ ?> 
												<div class="contentData">
													<div class="position-relative imageHover">
														<img src="<?php echo $audioImageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
														<div class="hoverItem w-100">
															<div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
																<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
																	<i aria-hidden="true" class="fa fa-play"></i>
																</a> 
																<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $audioImageURL; ?> ">
																	<i aria-hidden="true" class="fa fa-heart"></i>
																</a>   
															</div> 
														</div> 
													</div>
													<div class="itemDetails text-break">
														<a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
													</div>
												</div>
											<?php   } 
											?>
							

										<?php endwhile;
									endif;
									wp_reset_postdata(); ?>
								</div>
								</div>
							<?php } 
							if($imageContent){  ?>
								<div id="<?php echo $thirdTabContent ; ?>" role="tabpanel" class="tab-pane imageContent">
								<?php 
								if($RTL){ ?>
									<a href="<?php echo get_site_url(). '/ميديا?mediaType=image&cpt=' . $post_type->name . '&tname=' .  $custom_term->slug . '&xname=' . $custom_term->taxonomy ?>" class="moreBtn"><?php echo 'المزيد' ?></a> 
								<?php
								}else{?>
									<a href="<?php echo get_site_url(). '/media-taxonomy?mediaType=image&cpt=' . $post_type->name . '&tname=' .  $custom_term->slug . '&xname=' . $custom_term->taxonomy ?>" class="moreBtn"><?php echo 'More' ?></a> 
								<?php
								}?>

								<div class=" slickSlider  ">					
								<?php
									$args = array(
										'post_type'             => $post_type->name,
										'posts_per_page'        => -1, 
										'post_status'           => 'publish',
										'tax_query'             => array(
																	array(
																		'taxonomy' => $custom_term->taxonomy,
																		'field'    => 'slug',
																		'terms'    => $custom_term->slug,
																	),
																),
									);
									$_posts = new WP_Query( $args );

									if( $_posts->have_posts() ) :
										while( $_posts->have_posts() ) : $_posts->the_post(); 
											$postLink  	= get_post_permalink();
											$theTitle   = get_the_title();
											$theContent = get_the_content();
											$imageURL   = get_field('featured_image');
											$selectMedia   = get_field('select_media');
											global $wpdb; 
											$postID   		= get_the_ID();  
											$currentUserId	= get_current_user_id();
											$queryDB 		= 'select * from wp_user_favorite_posts WHERE fav_user_id = ' . $currentUserId  . ' AND fav_post_id = "' . $postID . '"' ;
											$results 		= $wpdb->get_results($queryDB);
											?>



											<?php  if($imageURL){ ?>
												<div class="contentData">
													<div class="position-relative imageHover">
														<img src="<?php echo $imageURL; ?>" class="w-100 h-100 objectCover " alt="" srcset=""> 
														<div class="hoverItem w-100">
															<div class="hoverBtn d-flex justify-content-center align-items-center h-100 text-center">
																<a class="sPlay-item eye" title="View " href="<?php echo $postLink ?>">
																	<i aria-hidden="true" class="fa fa-eye"></i>
																</a> 
																<a class="sFavrt-item <?php if($results){ ?> liked <?php } ?>" href="#" data-user-id="<?php echo $currentUserId; ?>" data-post-link="<?php echo $postLink; ?>" data-post-title="<?php echo $theTitle ?>" data-post-id="<?php echo $postID;?>" data-post-image="<?php echo $imageURL; ?> ">
																	<i aria-hidden="true" class="fa fa-heart"></i>
																</a>  
															</div> 
														</div> 
													</div>
													<div class="itemDetails text-break">
														<a href="<?php echo $postLink ?>"><?php echo $theTitle; ?></a>
													</div>
												</div>
											<?php }  ?>
										<?php endwhile;
									endif;
									wp_reset_postdata(); ?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php 
		
		$videoContent   = false;
		$audioContent   = false;
		$imageContent   = false;
	}

	}
?>
  
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?> 