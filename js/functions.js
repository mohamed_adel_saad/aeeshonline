/* global screenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

( function( $ ) {
	var body, masthead, menuToggle, siteNavigation, socialNavigation, siteHeaderMenu, resizeTimer, menuClose;

	function initMainNavigation( container ) {

		// Add dropdown toggle that displays child menu items.
		var dropdownToggle = $( '<button />', {
			'class': 'dropdown-toggle',
			'aria-expanded': false
		} ).append( $( '<span />', {
			'class': 'screen-reader-text',
			text: screenReaderText.expand
		} ) );

		container.find( '.menu-item-has-children > a' ).after( dropdownToggle );

		// Toggle buttons and submenu items with active children menu items.
		container.find( '.current-menu-ancestor > button' ).addClass( 'toggled-on' );
		container.find( '.current-menu-ancestor > .sub-menu' ).addClass( 'toggled-on' );

		// Add menu items with submenus to aria-haspopup="true".
		container.find( '.menu-item-has-children' ).attr( 'aria-haspopup', 'true' );

		container.find( '.dropdown-toggle' ).click( function( e ) {
			var _this            = $( this ),
				screenReaderSpan = _this.find( '.screen-reader-text' );

			e.preventDefault();
			_this.toggleClass( 'toggled-on' );
			_this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );

			// jscs:disable
			_this.attr( 'aria-expanded', _this.attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
			// jscs:enable
			screenReaderSpan.text( screenReaderSpan.text() === screenReaderText.expand ? screenReaderText.collapse : screenReaderText.expand );
		} );
	}
	initMainNavigation( $( '.main-navigation' ) );

	masthead         = $( '#masthead' );
	menuToggle       = masthead.find( '#menu-toggle' );
	siteHeaderMenu   = masthead.find( '#site-header-menu' );
	siteNavigation   = masthead.find( '#site-navigation' );
	socialNavigation = masthead.find( '#social-navigation' );
	menuClose		 = masthead.find( '#menu-close' );

	// Enable menuToggle.
	( function() {

		// Return early if menuToggle is missing.
		if ( ! menuToggle.length ) {
			return;
		}

		// Add an initial values for the attribute.
		menuToggle.add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded', 'false' );

		menuToggle.on( 'click.twentysixteen', function() {
			$( this ).add( siteHeaderMenu ).addClass( 'toggled-on' ); 
			// jscs:disable
			$( this ).add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded', $( this ).add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
			// jscs:enable
		} );

		menuClose.on( 'click.twentysixteen', function() {
			menuToggle.add( siteHeaderMenu ).removeClass( 'toggled-on' );
  
		} );
	} )();

	// Fix sub-menus for touch devices and better focus for hidden submenu items for accessibility.
	( function() {
		if ( ! siteNavigation.length || ! siteNavigation.children().length ) {
			return;
		}

		// Toggle `focus` class to allow submenu access on tablets.
		function toggleFocusClassTouchScreen() {
			if ( window.innerWidth >= 910 ) {
				$( document.body ).on( 'touchstart.twentysixteen', function( e ) {
					if ( ! $( e.target ).closest( '.main-navigation li' ).length ) {
						$( '.main-navigation li' ).removeClass( 'focus' );
					}
				} );
				siteNavigation.find( '.menu-item-has-children > a' ).on( 'touchstart.twentysixteen', function( e ) {
					var el = $( this ).parent( 'li' );

					if ( ! el.hasClass( 'focus' ) ) {
						e.preventDefault();
						el.toggleClass( 'focus' );
						el.siblings( '.focus' ).removeClass( 'focus' );
					}
				} );
			} else {
				siteNavigation.find( '.menu-item-has-children > a' ).unbind( 'touchstart.twentysixteen' );
			}
		}

		if ( 'ontouchstart' in window ) {
			$( window ).on( 'resize.twentysixteen', toggleFocusClassTouchScreen );
			toggleFocusClassTouchScreen();
		}

		siteNavigation.find( 'a' ).on( 'focus.twentysixteen blur.twentysixteen', function() {
			$( this ).parents( '.menu-item' ).toggleClass( 'focus' );
		} );
	} )();

	// Add the default ARIA attributes for the menu toggle and the navigations.
	function onResizeARIA() {
		if ( window.innerWidth < 910 ) {
			if ( menuToggle.hasClass( 'toggled-on' ) ) {
				menuToggle.attr( 'aria-expanded', 'true' );
			} else {
				menuToggle.attr( 'aria-expanded', 'false' );
			}

			if ( siteHeaderMenu.hasClass( 'toggled-on' ) ) {
				siteNavigation.attr( 'aria-expanded', 'true' );
				socialNavigation.attr( 'aria-expanded', 'true' );
			} else {
				siteNavigation.attr( 'aria-expanded', 'false' );
				socialNavigation.attr( 'aria-expanded', 'false' );
			}

			menuToggle.attr( 'aria-controls', 'site-navigation social-navigation' );
		} else {
			menuToggle.removeAttr( 'aria-expanded' );
			siteNavigation.removeAttr( 'aria-expanded' );
			socialNavigation.removeAttr( 'aria-expanded' );
			menuToggle.removeAttr( 'aria-controls' );
		}
	}

	// Add 'below-entry-meta' class to elements.
	function belowEntryMetaClass( param ) {
		if ( body.hasClass( 'page' ) || body.hasClass( 'search' ) || body.hasClass( 'single-attachment' ) || body.hasClass( 'error404' ) ) {
			return;
		}

		$( '.entry-content' ).find( param ).each( function() {
			var element              = $( this ),
				elementPos           = element.offset(),
				elementPosTop        = elementPos.top,
				entryFooter          = element.closest( 'article' ).find( '.entry-footer' ),
				entryFooterPos       = entryFooter.offset(),
				entryFooterPosBottom = entryFooterPos.top + ( entryFooter.height() + 28 ),
				caption              = element.closest( 'figure' ),
				figcaption           = element.next( 'figcaption' ),
				newImg;

			// Add 'below-entry-meta' to elements below the entry meta.
			if ( elementPosTop > entryFooterPosBottom ) {

				// Check if full-size images and captions are larger than or equal to 840px.
				if ( 'img.size-full' === param || '.wp-block-image img' === param ) {

					// Create an image to find native image width of resized images (i.e. max-width: 100%).
					newImg = new Image();
					newImg.src = element.attr( 'src' );

					$( newImg ).on( 'load.twentysixteen', function() {
						if ( newImg.width >= 840 ) {

							// Check if an image in an image block has a width attribute; if its value is less than 840, return.
							if ( '.wp-block-image img' === param && element.is( '[width]' ) && element.attr( 'width' ) < 840 ) {
								return;
							}

							element.addClass( 'below-entry-meta' );

							if ( caption.hasClass( 'wp-caption' ) ) {
								caption.addClass( 'below-entry-meta' );
								caption.removeAttr( 'style' );
							}

							if ( figcaption ) {
								figcaption.addClass( 'below-entry-meta' );
							}
						}
					} );
				} else {
					element.addClass( 'below-entry-meta' );
				}
			} else {
				element.removeClass( 'below-entry-meta' );
				caption.removeClass( 'below-entry-meta' );
			}
		} );
	}

	$( document ).ready( function() {
		body = $( document.body );
		const RTL = !!$("body").hasClass("rtl"); // Check if the current page is RTL or LTR.

		$( window )
			.on( 'load.twentysixteen', onResizeARIA )
			.on( 'resize.twentysixteen', function() {
				clearTimeout( resizeTimer );
				resizeTimer = setTimeout( function() {
					belowEntryMetaClass( 'img.size-full' );
					belowEntryMetaClass( 'blockquote.alignleft, blockquote.alignright' );
					belowEntryMetaClass( '.wp-block-image img' );
				}, 300 );
				onResizeARIA();
			} );

		belowEntryMetaClass( 'img.size-full' );
		belowEntryMetaClass( 'blockquote.alignleft, blockquote.alignright' );
		belowEntryMetaClass( '.wp-block-image img' );  

		$('.nav.tablist li:first-of-type a').addClass('active');
		$('.tab-content div:first-of-type').addClass('active');

		if(RTL){   
			$('.slickSlider').slick({
				slidesToShow: 5,
				slidesToScroll: 5,
				speed: 600,  
				infinite: false,   
				prevArrow: '<span class="leftArrow"><i class="fas fa-chevron-left"></i></span>',
				nextArrow: '<span class="rightArrow"><i class="fas fa-chevron-right"></i></span>',
				responsive: [
					{ breakpoint: 1190,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 4, 
						}
					},
					{ breakpoint: 600,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
					{ breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				],    
				rtl: true,
			});   

			$('.homeSlider').slick({
				centerMode: true,
				rtl: true,
				autoplay: true,
				autoplaySpeed: 3000,
				centerPadding: '60px',
				slidesToShow: 3,
				responsive: [
					{
					breakpoint: 768,
					settings: {
						centerPadding: '40px',
						slidesToShow: 3
					}
					},
					{
					breakpoint: 480,
					settings: {
						centerPadding: '40px',
						slidesToShow: 1
					}
					}
				],
				
			});


		} else{ 
			$('.slickSlider').slick({  
				slidesToShow: 5,
				slidesToScroll: 5,
				speed: 600,  
				infinite: false,   
				prevArrow: '<span class="leftArrow"><i class="fas fa-chevron-left"></i></span>',
				nextArrow: '<span class="rightArrow"><i class="fas fa-chevron-right"></i></span>',
				responsive: [
					{ breakpoint: 1190,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 4, 
						}
					},
					{ breakpoint: 600,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
					{ breakpoint: 480,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				], 
			}); 
			
			$('.homeSlider').slick({
				centerMode: true,
				infinite: true,
				 autoplay: true,
				 autoplaySpeed: 3000,
				adaptiveHeight: false,
				slidesToShow: 3,
				onInit: function () {
					$('.slick-current').prev().addClass('slickPrev');
					$('.slick-current').next().addClass('slickNext');
				}, 
				onBeforeChange: function () {
					alert("test");
					$('.slick-slide').removeClass('slickPrev slickNext');
				}, 
				onAfterChange: function () {
					$('.slick-current').prev().addClass("slickPrev");
					$('.slick-current').next().addClass("slickNext");
				},
				responsive: [
					{
					breakpoint: 768,
					settings: {
						centerPadding: '40px',
						slidesToShow: 3
					}
					},
					{
					breakpoint: 480,
					settings: {
						centerPadding: '40px',
						slidesToShow: 1
					}
					}
				],
				
			});
 
		}
	
		$('.openModal').click(function(){
			$('#myModal').modal('show');  
		});

		$('[data-fancybox]').fancybox({
			toolbar  : false,
			smallBtn : true,
			iframe : {
				preload : false
			}
		});  
 
		$(".comment-form-comment label").text("* Type your comment here"); 
		$(".comment-reply-title").text("Leave a Comment");
		$(".comment-form-comment + p input[name=submit]").val("Submit Comment");

		if(RTL){ 
			$(".says").text("يقول:");
			$(".comment-reply-link").text("رد");
			$(".comment-reply-title").text("اترك تعليقا");
			$(".comment-form-comment label").text("اكتب تعليقك هنا *"); 
			$(".comment-form-comment + p input[name=submit]").val("انشر التعليق");
			$(".comment-awaiting-moderation").text("تعليقك ينتظر الموافقة عليه. هذه معاينة ، سيكون تعليقك مرئيًا بعد الموافقة عليه.");
		} 
		$(".logged-in-as").remove(); 
		
		// Handling the comment form in single
		$(".form-submit #submit").addClass("disabledBtn"); 
		$("#commentform").on('input', function (e) {  
			const commentValue = $("#comment").val();
			if (commentValue.length >= 0) {
				e.preventDefault();
				$(".form-submit #submit").removeClass("disabledBtn"); 
			}
		}); 
		$("#commentform").on('submit', function (e) {  
			const commentValue = $("#comment").val();
			if (commentValue.length == 0) {
				e.preventDefault(); 
			}
		});
 

		// Handling the search form in header
		$(".search-form").on('submit', function (e) {
			const searchValue = $(".search-field").val().trim();
			$(".search-field").val(searchValue);

			if (searchValue.length == 0) {
				e.preventDefault();
			}
		}); 


		// Ajax call to like posts.
		const addToListBtn = $(".sFavrt-item:not('.liked')"); 
		addToListBtn.live("click", e => { 
			e.preventDefault();
			const currentTarget = e.currentTarget;
			const userID = $(currentTarget).attr("data-user-id");
			const postID = $(currentTarget).attr("data-post-id");
			const postLink = $(currentTarget).attr("data-post-link");
			const postTitle = $(currentTarget).attr("data-post-title");
			const postImage = $(currentTarget).attr("data-post-image");

			$.ajax({
				type: "POST",
				url: vl_ajax_script.ajaxurl,
				data: {
					action: "add_post_to_list_ajax_hook",
					userID: userID,
					postID: postID,
					postLink: postLink,
					postTitle: postTitle,
					postImage: postImage
				},
				success: function(data) { 
					const dataJSON = JSON.parse(data);
					var checkInsertStatus = dataJSON.check_insert_status; 
					var successInsert = dataJSON.successInsert;  
					console.log("successInsert = " + checkInsertStatus); 
					if (checkInsertStatus == true && successInsert !=false) {
						$(currentTarget).addClass("liked"); 
					}
				}
			});
		});


		// Ajax call to unlike posts.
		const removeFromListBtn = $(".liked");
		removeFromListBtn.live("click", e => {
			e.preventDefault();
			const currentTarget = e.currentTarget;
			const userID = $(currentTarget).attr("data-user-id");
			const postID = $(currentTarget).attr("data-post-id"); 

			$.ajax({
				type: "POST",
				url: vl_ajax_script.ajaxurl,
				data: {
					action: "delete_post_from_list_ajax_hook",
					postID: postID,
					userID: userID,
				},
				success: function(data) {  
					const dataJSON = JSON.parse(data);
					var deleteResult = dataJSON.deleteResult; 
					console.log("deleteResult = " + deleteResult); 
										
					if (deleteResult == true) {
						$(currentTarget).removeClass("liked");
					}
				}
			});
		});

		$(".menu-item.menu-item-has-children").append("<i class='fa fa-angle-right'></i>"); 

		/*-- Scroll Up/Down add class --*/
		var lastScrollTop = 0;
		$(window).scroll(function(event){
		var st = $(this).scrollTop();
		if (st >= lastScrollTop + 3 ){  
			$('.site-header').addClass('nav-up'); 
		} else {   
			$('.site-header').removeClass('nav-up');
		}
			lastScrollTop = st;
		});

 

	} );
} )( jQuery );
