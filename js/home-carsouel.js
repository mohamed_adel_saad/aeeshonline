jQuery(document).ready(function($){
    //image_slider in hero section in home page
    $('.image-rotator').hiSlide({
        interval: 50000,
        speed: 800, 
        item:'.hiSlider-item',  
        isFullScreen:false,  
        isFlexible:false,
    }); 
    
    $('.primary-menu.owl-carousel').owlCarousel({
        loop:false,
        margin:5,
        nav:true,
        dots:false, 
        autoWidth:false,
        rewind: true,
        center:false, 
        responsive:{
            0:{
                items:1
            },
            500:{
                items:2
            },
            700:{
                items:3
            },
            950:{
                items:4
            },
            1200:{
                items:5
            },
            1400:{
                items:6 
            }
        }
    }); 
  
    $( ".menu-item" ).click(function() {
        $(".menu-item.active").removeClass("active");  
        $(this).addClass("active"); 
        // $(this).toggleClass("active"); 
    });
  }); 